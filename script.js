// let http = require('http');

// 	let directory = [
// 			{
// 				"name" : "Brandon",
// 				"email" : "brandon@mail.com"
// 			},
// 			{
// 				"name" : "Jobert",
// 				"email" : "jobert@mail.com"
// 			}
// 		]
// 	const server = http.createServer((request,response) => {

// 		if(request.url == "/users" && request.method == "GET") {
// 			response.writeHead(200, {'Content-Type' : 'application/json'});
// 			response.write(JSON.stringify(directory));
// 			response.end();
// 		}

// 		if(request.url == "/users" && request.method == "POST") {

// 			let requestBody = "";

// 			request.on('data', function(data) {
// 				requestBody += data; 
// 			});

// 			request.on('end', function(){
// 				console.log(typeof requestBody);

// 				requestBody = JSON.parse(requestBody);

// 				let newUser = {
// 					"name" : requestBody.name,
// 					"email" : requestBody.email
// 				}

// 				directory.push(newUser);
// 				console.log(directory);

// 				response.writeHead(200, {'Content-Type' : 'application/json'});
// 				response.write(JSON.stringify(newUser));
// 				response.end();
// 			});
// 		}
// 	}).listen(4000);

// 	console.log("Server running at localhost: 4000");

let http = require("http");
const port = 4444;

http.createServer(function(request, response){

	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Welcome to Booking System');
	} else if(request.url == "/profile" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Welcome to your profile!');
	} else if(request.url == "/courses" && request.method == "GET") {		
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Here\'s our courses available');
	} else if(request.url == "/add-course" && request.method == "POST") {		
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Add a course to our resources');
	}

}).listen(4444);

console.log('Server running...');